<?php

/**
 * This is the model class for table "srep_phrases".
 *
 * The followings are the available columns in table 'srep_phrases':
 * @property string $id
 * @property string $phrase
 * @property integer $chast
 * @property string $id_group
 *
 * The followings are the available model relations:
 * @property SrepGroup $idGroup
 * @property SrepPosition[] $srepPositions
 */
class SrepPhrases extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'srep_phrases';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('phrase, id_group', 'required'),
			array('chast', 'numerical', 'integerOnly'=>true),
			array('phrase', 'length', 'max'=>1000),
			array('id_group', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, phrase, chast, id_group', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idGroup' => array(self::BELONGS_TO, 'SrepGroup', 'id_group'),
			'srepPositions' => array(self::HAS_MANY, 'SrepPosition', 'id_phrase'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'phrase' => 'Фраза',
			'chast' => 'Частота',
			'id_group' => 'Группа',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('phrase',$this->phrase,true);
		$criteria->compare('chast',$this->chast);
		$criteria->compare('id_group',$this->id_group,true);

		return new CActiveDataProvider($this, 
		    array(
			'criteria'=>$criteria,
			'pagination'=>array('pageSize'=>100),
		    )
		);
	}
	public function GroupChoices()
        {
            return CHtml::listData(SrepGroup::model()->findAll(), 'id', 'gr_name');
        }
	
	public function attributeWidgets()
        {
        return array(
            array('id_group','choosen'),
		);
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SrepPhrases the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
