<?php

/**
 * This is the model class for table "srep_position".
 *
 * The followings are the available columns in table 'srep_position':
 * @property string $id_phrase
 * @property integer $pos_goog
 * @property integer $pos_ya
 * @property string $date
 *
 * The followings are the available model relations:
 * @property SrepPhrases $idPhrase
 */
class SrepPosition extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'srep_position';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_phrase, date', 'required'),
			array('pos_goog, pos_ya', 'numerical', 'integerOnly'=>true),
			array('id_phrase', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_phrase, pos_goog, pos_ya, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPhrase' => array(self::BELONGS_TO, 'SrepPhrases', 'id_phrase'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_phrase' => 'Id Phrase',
			'pos_goog' => 'Pos Goog',
			'pos_ya' => 'Pos Ya',
			'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_phrase',$this->id_phrase,true);
		$criteria->compare('pos_goog',$this->pos_goog);
		$criteria->compare('pos_ya',$this->pos_ya);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SrepPosition the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
