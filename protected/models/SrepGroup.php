<?php

/**
 * This is the model class for table "srep_group".
 *
 * The followings are the available columns in table 'srep_group':
 * @property string $id
 * @property string $id_project
 * @property string $id_seobudget
 * @property string $gr_name
 *
 * The followings are the available model relations:
 * @property SrepProject $idProject
 * @property SrepIndex[] $srepIndexes
 * @property SrepPhrases[] $srepPhrases
 * @property string $fcsv
 */
class SrepGroup extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'srep_group';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_project,gr_name,id_seobudget', 'required'),
			array('id_project, id_seobudget', 'length', 'max'=>20),
			array('gr_name', 'length', 'max'=>255),
			array('fcsv', 'file', 'types'=>'csv', 'allowEmpty'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_project, id_seobudget, gr_name, fcsv', 'safe', 'on'=>'search'),
			array('fcsv', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idProject' => array(self::BELONGS_TO, 'SrepProject', 'id_project'),
			'srepIndexes' => array(self::HAS_MANY, 'SrepIndex', 'id_group'),
			'srepPhrases' => array(self::HAS_MANY, 'SrepPhrases', 'id_group'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_project' => 'Проект',
			'id_seobudget' => 'Id Seobudget',
			'gr_name' => 'Название группы',
			'fcsv' => 'Импорт фраз(.csv)',
		);
	}
	
	public static function load_csv($fileName,$id_group){
	    try {
		$csv = new Csv(YiiBase::getPathOfAlias('webroot').'/csv/'.$fileName);
		
		$get_csv = $csv->getCSV();
		if(!empty($get_csv)){
		    SrepPhrases::model()->deleteAll('id_group='.$id_group);
		    foreach ($get_csv as $value) {
			if($value[0]=="" || $value[0]=="Фраза"){
			    continue;
			}
			$model=new SrepPhrases;
			$model->attributes = array(
			    'phrase'=> $value[0],
			    'chast'=> $value[1],
			    'id_group'=> $id_group
			);
			$model->save();
		    }
		}
	    }
	    catch (Exception $e) {
		echo "Ошибка: " . $e->getMessage();
	    }
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('id_project',$this->id_project,true);
		$criteria->compare('id_seobudget',$this->id_seobudget,true);
		$criteria->compare('gr_name',$this->gr_name,true);
		$criteria->compare('fcsv',$this->fcsv,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function ProjectChoices()
        {
            return CHtml::listData(SrepProject::model()->findAll(), 'id', 'pr_name');
        }
	
	public function attributeWidgets()
        {
        return array(
            array('id_project','choosen'),
		);
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SrepGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
