<?php
class ReportController extends Controller
{
	public function actionView($id)
	{
		Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/report.css');
		/*Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js/tooltipster/css/tooltipster.css');
		Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js/tooltipster/css/themes/tooltipster-shadow.css');
		Yii::app()->getClientScript()->registerCoreScript('jquery');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/tooltipster/js/jquery.tooltipster.min.js', CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/scriptReport.js', CClientScript::POS_END);*/
		$report= new Report($id);
		$reportData = $report->create_report();
		$project = SrepProject::model()->findByPk($id);
		$sql_date = "SELECT DISTINCT `si`.`date` FROM `srep_index` `si`
		    INNER JOIN `srep_group` `sg` ON `sg`.`id` = `si`.`id_group`
		    WHERE `sg`.`id_project` = :project_id
		    ORDER BY `si`.`date` DESC
		    LIMIT 2
		    ";

		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql_date);
		$command->bindParam(":project_id",$id,PDO::PARAM_STR);
		$last_date = $command->queryColumn();
		$this->render('view',array(
			'reportData' => $reportData,
			'project' => $project,
			'date_up' =>$last_date
		));
	}
	
	
}