<?php
class ProjectController extends Controller
{
	public $layout='//layouts/column2';
    	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('setposition','view'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionView($id)
	{
		Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/dxcharts/jquery-2.0.3.min.js');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/dxcharts/globalize.min.js', CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/dxcharts/dx.chartjs.js', CClientScript::POS_END);
		$reportData = $this->get_index_project($id);
		$this->render('view',array(
			'reportData' => $reportData,
			'id' => $id
		));
	}
	
	public function actionSetPosition(){
	    //print YiiBase::getPathOfAlias('application.components');die;
		$day = date('d',time());

		$current_date =  mktime(0,0,0,date('m',time()), $day-18, date('Y',time()));
		$last_date =  mktime(0,0,0,date('m',time()), $day-16, date('Y',time()));
		$report = new SeoBud($current_date, $last_date);
		$report->sb_set_position();
	}
	
	public function get_index_project($project_id){
		$sql = "SELECT * FROM `srep_index` `si`
			INNER JOIN `srep_group` `sg` ON `sg`.`id` = `si`.`id_group`
			INNER JOIN `srep_project` `spr` ON `spr`.`id` = `sg`.`id_project`
			WHERE `sg`.`id_project` = :project_id
			ORDER BY `sg`.`id_project`, `si`.`date`
		";
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":project_id",$project_id,PDO::PARAM_STR);
		$result = $command->queryAll();
		foreach($result as $data){
		    
			$fdate = Yii::app()->dateFormatter->formatDateTime($data['date'],'full',null);
			$indexses['groups'][$data['id_group']][$fdate]['name'] = $data['gr_name'];
			$indexses['groups'][$data['id_group']][$fdate]['ya'] = (int)$data['ya_index'];
			$indexses['groups'][$data['id_group']][$fdate]['goog'] = (int)$data['goog_index'];
			$indexses['groups'][$data['id_group']][$fdate]['sum'] = (int)$data['goog_index']+(int)$data['ya_index'];
			$indexses['groups'][$data['id_group']][$fdate]['date'] = $fdate;
			$indexses['projects'][$data['id_project']][$fdate]['name'] = $data['pr_name'];
			$indexses['projects'][$data['id_project']][$fdate]['ya'] += (int)$data['ya_index'];
			$indexses['projects'][$data['id_project']][$fdate]['goog'] += (int)$data['goog_index'];
			$indexses['projects'][$data['id_project']][$fdate]['sum'] += (int)$data['goog_index']+(int)$data['ya_index'];
			$indexses['projects'][$data['id_project']][$fdate]['date'] = $fdate;
			$project_name = $data['pr_name'];
		}
		$data['data'] = $indexses;
		$data['pr_name'] = $project_name;
		return $data;
	}
}