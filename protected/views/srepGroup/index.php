<?php
/* @var $this SrepGroupController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Группы',
);

$this->menu=array(
	array('label'=>'Добавить группу', 'url'=>array('create')),
	array('label'=>'Управление группами', 'url'=>array('admin')),
);
?>

<h1>Группы</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
