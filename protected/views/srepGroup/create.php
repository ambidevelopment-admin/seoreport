<?php
/* @var $this SrepGroupController */
/* @var $model SrepGroup */

$this->breadcrumbs=array(
	'Группы'=>array('index'),
	'Добавлние группы',
);

$this->menu=array(
	array('label'=>'Список групп', 'url'=>array('index')),
	array('label'=>'Управления группами', 'url'=>array('admin')),
);
?>

<h1>Добавлние группы</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>