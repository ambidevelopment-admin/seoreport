<?php
/* @var $this SrepGroupController */
/* @var $data SrepGroup */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_project')); ?>:</b>
	<?php echo CHtml::encode($data->id_project); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_seobudget')); ?>:</b>
	<?php echo CHtml::encode($data->id_seobudget); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gr_name')); ?>:</b>
	<?php echo CHtml::encode($data->gr_name); ?>
	<br />


</div>