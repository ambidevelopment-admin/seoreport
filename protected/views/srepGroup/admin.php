<?php
/* @var $this SrepGroupController */
/* @var $model SrepGroup */

$this->breadcrumbs=array(
	'Группы'=>array('index'),
	'Управление группами',
);

$this->menu=array(
	array('label'=>'Список групп', 'url'=>array('index')),
	array('label'=>'Добавить группу', 'url'=>array('create')),
);

?>

<h1>Управление группами</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'srep-group-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array(
			'name'=>'id_project',
			'value'=>'$data->idProject->pr_name',
			'filter'=>'',
		),
		'id_seobudget',
		'gr_name',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
