<?php
/* @var $this SrepGroupController */
/* @var $model SrepGroup */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'srep-group-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype' => 'multipart/form-data')
)); ?>

	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php echo $form->labelEx($model,'gr_name'); ?>
		<?php echo $form->textField($model,'gr_name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'gr_name'); ?>
	</div>
    
	<div class="row">
		<?php echo $form->dropDownList($model,'id_project',$model->ProjectChoices(),array('prompt'=>'-Выберите проект-')); ?>
		<?php echo $form->error($model,'id_project'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_seobudget'); ?>
		<?php echo $form->textField($model,'id_seobudget',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'id_seobudget'); ?>
	</div>
	<?php if($model->isNewRecord!='1'): ?>
	<div class="row">
		<?php echo $form->labelEx($model,'fcsv'); ?>
		<?php echo $form->fileField($model, 'fcsv'); ?>
	</div>
	<?php endif;?>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->