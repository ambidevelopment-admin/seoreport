<?php
/* @var $this SrepGroupController */
/* @var $model SrepGroup */

$this->breadcrumbs=array(
	'Группы'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	$model->gr_name,
);

$this->menu=array(
	array('label'=>'Список групп', 'url'=>array('index')),
	array('label'=>'Создать группу', 'url'=>array('create')),
	array('label'=>'Просмотр группы', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление группами', 'url'=>array('admin')),
);
?>

<h1><?php echo $model->gr_name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>