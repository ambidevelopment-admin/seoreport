<?php
/* @var $this SrepGroupController */
/* @var $model SrepGroup */

$this->breadcrumbs=array(
	'Группы'=>array('index'),
	$model->gr_name,
);

$this->menu=array(
	array('label'=>'Список групп', 'url'=>array('index')),
	array('label'=>'Добавить группу', 'url'=>array('create')),
	array('label'=>'Изменить группу', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить группу', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление группами', 'url'=>array('admin')),
);
?>

<h1><?php echo $model->gr_name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_project',
		'id_seobudget',
		'gr_name',
	),
)); ?>
