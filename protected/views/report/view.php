<?php 
$this->breadcrumbs=array(
	$project['pr_name'],
);

$count = count($reportData);
?>
<p class="date-up">Отчет <?php echo Yii::app()->dateFormatter->formatDateTime($date_up[1],'medium',null) ." — ".Yii::app()->dateFormatter->formatDateTime($date_up[0],'medium',null)?></p>
<table class="report-tbl">
    <thead>
	<tr class="top-tr"> 
	    <th></th>
	    <th colspan=3>Эффективных показов</th> 
	    <th colspan=3>Позиции в Яндекс</th> 
	    <th colspan=3>Позиции в Google</th> 
	    <th colspan=3>Видимость</th> 
	    <th>График</th> 
	</tr>
	<tr>
	    <th></th>
	    <th>Всего</th>
	    <th>Яндекс</th>
	    <th>Google</th>
	    <th>1-3</th>
	    <th>4-10</th>
	    <th>11<</th>
	    <th>1-3</th>
	    <th>4-10</th>
	    <th>11<</th>
	    <th>Общая</th>
	    <th>в Яндекс</th>
	    <th>в Google</th>
	    <th></th>
	</tr>
    </thead>
    <tbody>
	
	<?php 
	    $out = "";
	    foreach($reportData as $gr_name=>$data){	   
		$out .=  "<tr>";
		if($gr_name=='project') $gr_name = $project['pr_name'];
		$out .=  "<td class='name'>".$gr_name."</td>";
		
		$out .=  "<td>".$data['effect_sum']." (".positiv($data['effect_sum_raz']).")</td>";
		$out .=  "<td>".$data['effect_ya']." (".positiv($data['effect_ya_raz']).")</td>";
		$out .=  "<td>".$data['effect_goog']." (".positiv($data['effect_goog_raz']).")</td>";
		
		$out .=  "<td>".(($data['positions_ya'][1]) ? $data['positions_ya'][1] : 0)."</td>";
		$out .=  "<td>".(($data['positions_ya'][2]) ? $data['positions_ya'][2] : 0)."</td>";
		$out .=  "<td>".(($data['positions_ya'][3]) ? $data['positions_ya'][3] : 0)."</td>";
		
		$out .=  "<td>".(($data['positions_goog'][1]) ? $data['positions_goog'][1] : 0)."</td>";
		$out .=  "<td>".(($data['positions_goog'][2]) ? $data['positions_goog'][2] : 0)."</td>";
		$out .=  "<td>".(($data['positions_goog'][3]) ? $data['positions_goog'][3] : 0)."</td>";
		
		$out .=  "<td>".$data['view_pos_sum']."% (".positiv($data['view_pos_sum_raz'])."%)</td>";
		$out .=  "<td>".$data['view_pos_ya']."% (".positiv($data['view_pos_ya_raz'])."%)</td>";
		$out .=  "<td>".$data['view_pos_goog']."% (".positiv($data['view_pos_goog_raz'])."%)</td>";
		if(!empty($data['group_id'])){
		    $out .=  "<td>". CHtml::link('ссылка',$this->createUrl('/project/view', array('id'=>$project['id'], '#'=>'group_'.$data['group_id'])))."</td>";
		}else{
		    $out .=  "<td>". CHtml::link('ссылка',$this->createUrl('/project/view', array('id'=>$project['id'])))."</td>";
		}
		$out .=  "</tr>";
	    }
	    print $out;
	?>
		
    </tbody>
</table>