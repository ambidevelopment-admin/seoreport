<?php
/* @var $this SrepPhrasesController */
/* @var $model SrepPhrases */

$this->breadcrumbs=array(
	'Фразы'=>array('index'),
	'Управление фразами',
);

$this->menu=array(
	array('label'=>'Список фраз', 'url'=>array('index')),
	array('label'=>'Добавить фразу', 'url'=>array('create')),
);

?>

<h1>Фразы</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'srep-phrases-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'phrase',
		'chast',
		array(
			'name'=>'id_group',
			'value'=>'$data->idGroup->gr_name',
			'filter'=>'',
		),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
