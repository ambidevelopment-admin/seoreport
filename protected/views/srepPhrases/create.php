<?php
/* @var $this SrepPhrasesController */
/* @var $model SrepPhrases */

$this->breadcrumbs=array(
	'Фразы'=>array('index'),
	'Добавление фразы',
);

$this->menu=array(
	array('label'=>'Список фраз', 'url'=>array('index')),
	array('label'=>'Управление фразами', 'url'=>array('admin')),
);
?>

<h1>Добавление фразы</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>