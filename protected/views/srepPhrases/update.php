<?php
/* @var $this SrepPhrasesController */
/* @var $model SrepPhrases */

$this->breadcrumbs=array(
	'Фразы'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	$model->phrase,
);

$this->menu=array(
	array('label'=>'Список фраз', 'url'=>array('index')),
	array('label'=>'Добавление фразы', 'url'=>array('create')),
	array('label'=>'Просмотр фразы', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление фразами', 'url'=>array('admin')),
);
?>

<h1><?php echo $model->phrase; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>