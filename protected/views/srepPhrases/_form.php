<?php
/* @var $this SrepPhrasesController */
/* @var $model SrepPhrases */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'srep-phrases-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'phrase'); ?>
		<?php echo $form->textField($model,'phrase',array('size'=>60,'maxlength'=>1000)); ?>
		<?php echo $form->error($model,'phrase'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'chast'); ?>
		<?php echo $form->textField($model,'chast'); ?>
		<?php echo $form->error($model,'chast'); ?>
	</div>

	<div class="row">
		<?php echo $form->dropDownList($model,'id_group',$model->GroupChoices(),array('prompt'=>'-Выберите группу-')); ?>
		<?php echo $form->error($model,'id_groupt'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->