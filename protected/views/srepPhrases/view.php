<?php
/* @var $this SrepPhrasesController */
/* @var $model SrepPhrases */

$this->breadcrumbs=array(
	'Srep Phrases'=>array('index'),
	$model->phrase,
);

$this->menu=array(
	array('label'=>'Список фраз', 'url'=>array('index')),
	array('label'=>'Добавление фразы', 'url'=>array('create')),
	array('label'=>'Изменить фразу', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить фразу', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление фразами', 'url'=>array('admin')),
);
?>

<h1>View SrepPhrases #<?php echo $model->phrase; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'phrase',
		'chast',
		'id_group',
	),
)); ?>
