<?php
/* @var $this SrepPhrasesController */
/* @var $data SrepPhrases */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phrase')); ?>:</b>
	<?php echo CHtml::encode($data->phrase); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('chast')); ?>:</b>
	<?php echo CHtml::encode($data->chast); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_group')); ?>:</b>
	<?php echo CHtml::encode($data->id_group); ?>
	<br />


</div>