<?php
/* @var $this SrepPhrasesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Фразы',
);

$this->menu=array(
	array('label'=>'Добавление фразы', 'url'=>array('create')),
	array('label'=>'Управление фразами', 'url'=>array('admin')),
);
?>

<h1>Srep Phrases</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
