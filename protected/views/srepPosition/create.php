<?php
/* @var $this SrepPositionController */
/* @var $model SrepPosition */

$this->breadcrumbs=array(
	'Srep Positions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SrepPosition', 'url'=>array('index')),
	array('label'=>'Manage SrepPosition', 'url'=>array('admin')),
);
?>

<h1>Create SrepPosition</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>