<?php
/* @var $this SrepPositionController */
/* @var $data SrepPosition */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_phrase')); ?>:</b>
	<?php echo CHtml::encode($data->id_phrase); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pos_goog')); ?>:</b>
	<?php echo CHtml::encode($data->pos_goog); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pos_ya')); ?>:</b>
	<?php echo CHtml::encode($data->pos_ya); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />


</div>