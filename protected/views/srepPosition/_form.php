<?php
/* @var $this SrepPositionController */
/* @var $model SrepPosition */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'srep-position-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_phrase'); ?>
		<?php echo $form->textField($model,'id_phrase',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'id_phrase'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pos_goog'); ?>
		<?php echo $form->textField($model,'pos_goog'); ?>
		<?php echo $form->error($model,'pos_goog'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pos_ya'); ?>
		<?php echo $form->textField($model,'pos_ya'); ?>
		<?php echo $form->error($model,'pos_ya'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->