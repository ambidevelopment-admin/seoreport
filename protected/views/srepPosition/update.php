<?php
/* @var $this SrepPositionController */
/* @var $model SrepPosition */

$this->breadcrumbs=array(
	'Srep Positions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SrepPosition', 'url'=>array('index')),
	array('label'=>'Create SrepPosition', 'url'=>array('create')),
	array('label'=>'View SrepPosition', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SrepPosition', 'url'=>array('admin')),
);
?>

<h1>Update SrepPosition <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>