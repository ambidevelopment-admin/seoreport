<?php
/* @var $this SrepPositionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Srep Positions',
);

$this->menu=array(
	array('label'=>'Create SrepPosition', 'url'=>array('create')),
	array('label'=>'Manage SrepPosition', 'url'=>array('admin')),
);
?>

<h1>Srep Positions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
