<?php
/* @var $this SrepPositionController */
/* @var $model SrepPosition */

$this->breadcrumbs=array(
	'Srep Positions'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List SrepPosition', 'url'=>array('index')),
	array('label'=>'Create SrepPosition', 'url'=>array('create')),
	array('label'=>'Update SrepPosition', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SrepPosition', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SrepPosition', 'url'=>array('admin')),
);
?>

<h1>View SrepPosition #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_phrase',
		'pos_goog',
		'pos_ya',
		'date',
	),
)); ?>
