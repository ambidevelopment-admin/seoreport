<?php
/* @var $this SrepIndexController */
/* @var $data SrepIndex */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_group')); ?>:</b>
	<?php echo CHtml::encode($data->id_group); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ya_index')); ?>:</b>
	<?php echo CHtml::encode($data->ya_index); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('goog_index')); ?>:</b>
	<?php echo CHtml::encode($data->goog_index); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />


</div>