<?php
/* @var $this SrepIndexController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Srep Indexes',
);

$this->menu=array(
	array('label'=>'Create SrepIndex', 'url'=>array('create')),
	array('label'=>'Manage SrepIndex', 'url'=>array('admin')),
);
?>

<h1>Srep Indexes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
