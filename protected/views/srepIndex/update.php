<?php
/* @var $this SrepIndexController */
/* @var $model SrepIndex */

$this->breadcrumbs=array(
	'Srep Indexes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SrepIndex', 'url'=>array('index')),
	array('label'=>'Create SrepIndex', 'url'=>array('create')),
	array('label'=>'View SrepIndex', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SrepIndex', 'url'=>array('admin')),
);
?>

<h1>Update SrepIndex <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>