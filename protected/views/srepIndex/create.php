<?php
/* @var $this SrepIndexController */
/* @var $model SrepIndex */

$this->breadcrumbs=array(
	'Srep Indexes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SrepIndex', 'url'=>array('index')),
	array('label'=>'Manage SrepIndex', 'url'=>array('admin')),
);
?>

<h1>Create SrepIndex</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>