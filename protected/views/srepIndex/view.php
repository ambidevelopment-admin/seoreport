<?php
/* @var $this SrepIndexController */
/* @var $model SrepIndex */

$this->breadcrumbs=array(
	'Srep Indexes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List SrepIndex', 'url'=>array('index')),
	array('label'=>'Create SrepIndex', 'url'=>array('create')),
	array('label'=>'Update SrepIndex', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SrepIndex', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SrepIndex', 'url'=>array('admin')),
);
?>

<h1>View SrepIndex #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_group',
		'ya_index',
		'goog_index',
		'date',
	),
)); ?>
