<?php
/* @var $this SrepIndexController */
/* @var $model SrepIndex */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'srep-index-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_group'); ?>
		<?php echo $form->textField($model,'id_group',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'id_group'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ya_index'); ?>
		<?php echo $form->textField($model,'ya_index'); ?>
		<?php echo $form->error($model,'ya_index'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'goog_index'); ?>
		<?php echo $form->textField($model,'goog_index'); ?>
		<?php echo $form->error($model,'goog_index'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->