<?php
/* @var $this SrepGroupController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	$reportData['pr_name'],
);

$this->menu=array(
        array('label'=>'Поисковые системы', 'items'=>array(
		array('label'=>'Все', 'url'=>array('#'), 'linkOptions'=>array('id'=>'allLine')),
		array('label'=>'Только Google', 'url'=>array('#'), 'linkOptions'=>array('id'=>'gLine')),
		array('label'=>'Только Yandex', 'url'=>array('#'), 'linkOptions'=>array('id'=>'yaLine')),
        )),
	array('label'=>'Суммарный график', 'url'=>array('#'), 'linkOptions'=>array('id'=>'viewAll')),
	array('label'=>'Вид графика', 'items'=>array(
	    array('label'=>'Столбцы', 'url'=>array('#'), 'linkOptions'=>array('id'=>'typeBar')),
	    array('label'=>'Линии', 'url'=>array('#'), 'linkOptions'=>array('id'=>'typeLine')),
	)),
	array('label'=>'Отчет', 'url'=>array('report/view/'.$id)),
);
?>

<script>
$(function () {
	json = <?php echo json_encode($reportData['data']);?>;
	$.each(json.groups, function(gr_id, dataGroup) {
		var iGr=0;
		var titleGr='';
		var dataSourceGr = [];
		var appendGr = '<a name="group_'+gr_id+'"></a><div id="chartContainerGroup'+gr_id+'" class="chart"></div>';
		$("#StatisticGr").append(appendGr);
		$.each(dataGroup, function(date, dataItem) {
			dataSourceGr[iGr] = dataItem;
			titleGr = dataItem.name;
			iGr++;
		});

		$("#chartContainerGroup"+gr_id).dxChart({
			palette:"Harmony Light",
			dataSource: dataSourceGr,
			commonSeriesSettings: {
				argumentField: "date",
				type: "spline",
				hoverMode: "allArgumentPoints",
				selectionMode: "allArgumentPoints",
				label: {
					visible: true,
					format: "fixedPoint",
					precision: 0
				}
			},
			series: [
				{ valueField: "goog", name: "Google", color: '#4485F5'},
				{ valueField: "ya", name: "Yandex", color: '#FFCC00' },
			],
			title: titleGr,
			pointClick: function (point) {
				this.select();
			}
		});

		$("#gLine").click(function(e) {
			e.preventDefault();
			var chart = $("#chartContainerGroup"+gr_id).dxChart("instance");
			chart.option({
				series: [ { valueField: "goog", name: "Google", color: '#4485F5'},]
			});
		});

		$("#yaLine").click(function(e) {
			e.preventDefault();	
			var chart = $("#chartContainerGroup"+gr_id).dxChart("instance");
			chart.option({
				series: [ { valueField: "ya", name: "Yandex", color: '#FFCC00' },]
			});
		});

		$("#allLine").click(function(e) {
			e.preventDefault();
			var chart = $("#chartContainerGroup"+gr_id).dxChart("instance");
			chart.option({
				series: [ { valueField: "ya", name: "Yandex", color: '#FFCC00' },{ valueField: "goog", name: "Google", color: '#4485F5'}]
			});
		});
		
		$("#viewAll").click(function(e) {
			e.preventDefault();
			var chart = $("#chartContainerGroup"+gr_id).dxChart("instance");
			chart.option({
				series: [ { valueField: "sum", name: "Общая", color: '#FF0000' }]
			});
		});
		
		$("#typeBar").click(function(e) {
			e.preventDefault();
			var chart = $("#chartContainerGroup"+gr_id).dxChart("instance");
			chart.option("commonSeriesSettings.type", "bar");
		});
		$("#typeLine").click(function(e) {
			e.preventDefault();
			var chart = $("#chartContainerGroup"+gr_id).dxChart("instance");
			chart.option("commonSeriesSettings.type", "spline");
		});			

	});
	$.each(json.projects, function(pr_id, dataProject) {
		var i=0;
		var title='';
		var dataSource = [];
		var append = '<div id="chartContainerProject'+pr_id+'" class="chart"></div>';
		$("#StatisticPr").append(append);
		$.each(dataProject, function(date, dataItem) {
			dataSource[i] = dataItem;
			title = dataItem.name;
			i++;
		});

		$("#chartContainerProject"+pr_id).dxChart({
			dataSource: dataSource,
			commonSeriesSettings: {
				argumentField: "date",
				type: "spline",
				hoverMode: "allArgumentPoints",
				selectionMode: "allArgumentPoints",
				label: {
					visible: true,
					format: "fixedPoint",
					precision: 0
				}
			},
			series: [
				{ valueField: "goog", name: "Google", color: '#4485F5'},
				{ valueField: "ya", name: "Yandex", color: '#FFCC00' },
			],
			title: title,
			pointClick: function (point) {
				this.select();
			}
		});

		$("#gLine").click(function() {
			var chart = $("#chartContainerProject"+pr_id).dxChart("instance");
			chart.option({
				series: [ { valueField: "goog", name: "Google", color: '#4485F5'},]
			});
		});

		$("#yaLine").click(function() {
			var chart = $("#chartContainerProject"+pr_id).dxChart("instance");
			chart.option({
				series: [ { valueField: "ya", name: "Yandex", color: '#FFCC00' },]
			});
		});
		
		$("#viewAll").click(function(e) {
			e.preventDefault();
			var chart = $("#chartContainerProject"+pr_id).dxChart("instance");
			chart.option({
				series: [ { valueField: "sum", name: "Общая", color: '#FF0000' }]
			});
		});

		$("#allLine").click(function() {
			var chart = $("#chartContainerProject"+pr_id).dxChart("instance");
			chart.option({
				series: [ { valueField: "ya", name: "Yandex", color: '#FFCC00' },{ valueField: "goog", name: "Google", color: '#4485F5'}]
			});
		});

		$("#typeBar").click(function() {
			var chart = $("#chartContainerProject"+pr_id).dxChart("instance");
			chart.option("commonSeriesSettings.type", "bar");
		});
		$("#typeLine").click(function() {
			var chart = $("#chartContainerProject"+pr_id).dxChart("instance");
			chart.option("commonSeriesSettings.type", "spline");
		});
	});

});	
</script>
<div class="mainWrap">
    <h2>Проект</h2>
    <div id="StatisticPr" style="max-width:700px;"></div>	
    <h2>Группы</h2>
    <div id="StatisticGr" style="max-width:700px;"></div>
</div>