<?php
/* @var $this SrepProjectController */
/* @var $model SrepProject */

$this->breadcrumbs=array(
	'Проекты'=>array('index'),
	'Управление проектами',
);

$this->menu=array(
	array('label'=>'Список проектов', 'url'=>array('index')),
	array('label'=>'Добавить проект', 'url'=>array('create')),
);

?>

<h1>Проекты</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'srep-project-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'pr_name',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
