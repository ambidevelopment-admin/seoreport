<?php
/* @var $this SrepProjectController */
/* @var $model SrepProject */

$this->breadcrumbs=array(
	'Проекты'=>array('index'),
	'Добавление проекта',
);

$this->menu=array(
	array('label'=>'Список проектов', 'url'=>array('index')),
	array('label'=>'Управление проектами', 'url'=>array('admin')),
);
?>

<h1>Добавление проекта</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>