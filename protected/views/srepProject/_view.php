<?php
/* @var $this SrepProjectController */
/* @var $data SrepProject */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pr_name')); ?>:</b>
	<?php echo CHtml::encode($data->pr_name); ?>
	<br />


</div>