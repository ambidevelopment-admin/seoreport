<?php
/* @var $this SrepProjectController */
/* @var $model SrepProject */

$this->breadcrumbs=array(
	'Srep Projects'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List SrepProject', 'url'=>array('index')),
	array('label'=>'Create SrepProject', 'url'=>array('create')),
	array('label'=>'Update SrepProject', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SrepProject', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SrepProject', 'url'=>array('admin')),
);
?>

<h1>View SrepProject #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'pr_name',
	),
)); ?>
