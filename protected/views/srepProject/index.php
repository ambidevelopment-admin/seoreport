<?php
/* @var $this SrepProjectController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Проекты',
);

$this->menu=array(
	array('label'=>'Добавление проекта', 'url'=>array('create')),
	array('label'=>'Управление проектами', 'url'=>array('admin')),
);
?>

<h1>Проекты</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
