<?php
/* @var $this SrepProjectController */
/* @var $model SrepProject */

$this->breadcrumbs=array(
	'Проекты'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	$model->pr_name,
);

$this->menu=array(
	array('label'=>'Список проектов', 'url'=>array('index')),
	array('label'=>'Добавление проекта', 'url'=>array('create')),
	array('label'=>'Просмотр проекта', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление проектами', 'url'=>array('admin')),
);
?>

<h1><?php echo $model->pr_name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>