<?php
class Report {
    
    public $id_project;
    
    public function __construct($id) {
	    $this->id_project = $id;
	    //$this->create_report();
    }
    
    public function create_report()
    {
	    $effect_view = $this->get_effect_view();
	    $position = $this->get_position();
	    $visibles = $this->get_visibles();
	    $respos = array_merge_recursive($effect_view,$position,$visibles);
	    function cmp ($a, $b)
	    {
		if ($a == 'project' || $b == 'project') return -1;
		return 1;
	    }
	    uksort($respos,'cmp');
	    return $respos;
    }    
    
    public function get_effect_view(){
		$sql = "
		    SELECT * FROM `srep_index` `si`
		    INNER JOIN `srep_group` `sg` ON `sg`.`id` = `si`.`id_group`
		    INNER JOIN `srep_project` `spr` ON `spr`.`id` = `sg`.`id_project`
		    WHERE `sg`.`id_project` = :project_id
		    ORDER BY `si`.`date` DESC
		";
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		$command->bindParam(":project_id",$this->id_project,PDO::PARAM_STR);
		$result = $command->queryAll();
		$date_points = 1;
		$check_fdate = Yii::app()->dateFormatter->formatDateTime($result[0]['date'],'full',null);
		for($i=0;$i<count($result);++$i){
		    $fdate = Yii::app()->dateFormatter->formatDateTime($result[$i]['date'],'full',null);
		    if($check_fdate<>$fdate){
			$check_fdate = Yii::app()->dateFormatter->formatDateTime($result[$i]['date'],'full',null);
			++$date_points;
		    }
		    if($date_points<3){
			$effect[$result[$i]['gr_name']][$date_points]['eff_ya'] = $result[$i]['ya_index'];
			$effect[$result[$i]['gr_name']][$date_points]['eff_goog'] = $result[$i]['goog_index'];
			$effect[$result[$i]['gr_name']][$date_points]['eff_sum'] = $result[$i]['goog_index']+$result[$i]['ya_index'];
			$effect['project'][$date_points]['eff_ya'] += $result[$i]['ya_index'];
			$effect['project'][$date_points]['eff_goog'] += $result[$i]['goog_index'];
			$effect['project'][$date_points]['eff_sum'] += $result[$i]['goog_index']+$result[$i]['ya_index'];
		    }else{
			break;
		    }
		}
		if(!empty($effect)){
		    foreach($effect as $gr_id=>$ef_data){
			$effect_rez[$gr_id]['effect_ya'] = $ef_data[1]['eff_ya'];
			$effect_rez[$gr_id]['effect_goog'] = $ef_data[1]['eff_goog'];
			$effect_rez[$gr_id]['effect_sum'] = $ef_data[1]['eff_sum'];
			$effect_rez[$gr_id]['effect_ya_raz'] = $ef_data[1]['eff_ya']-$ef_data[2]['eff_ya'];
			$effect_rez[$gr_id]['effect_goog_raz'] = $ef_data[1]['eff_goog']-$ef_data[2]['eff_goog'];
			$effect_rez[$gr_id]['effect_sum_raz'] = $ef_data[1]['eff_sum']-$ef_data[2]['eff_sum'];
		    }
		}
		return $effect_rez;
    }
    
    public function get_position(){
	    $sql_date = "SELECT MAX(`si`.`date`) FROM `srep_index` `si`
		    INNER JOIN `srep_group` `sg` ON `sg`.`id` = `si`.`id_group`
		    WHERE `sg`.`id_project` = :project_id
		    GROUP BY `si`.`date`";

	    $connection=Yii::app()->db;
	    $command=$connection->createCommand($sql_date);
	    $command->bindParam(":project_id",$this->id_project,PDO::PARAM_STR);
	    $last_date = $command->queryScalar();
	    $sql = "SELECT * FROM `srep_group` `sg`
		    INNER JOIN `srep_phrases` `sph` ON `sg`.`id` = `sph`.`id_group`
		    INNER JOIN `srep_position` `spos` ON `sph`.`id` = `spos`.`id_phrase`
		    WHERE `sg`.`id_project` = :project_id";

	    $command=$connection->createCommand($sql);
	    $command->bindParam(":project_id",$this->id_project,PDO::PARAM_STR);
	    $result = $command->queryAll();
	    foreach($result as $pos){
		$positions[$pos['gr_name']]['group_id'] = $pos['id_group'];
		if($pos['pos_goog']<4 && $pos['pos_goog']<>0){
		    $positions[$pos['gr_name']]['positions_goog'][1]++;
		    $positions['project']['positions_goog'][1]++;
		}
		if($pos['pos_ya']<4 && $pos['pos_ya']<>0){
		    $positions[$pos['gr_name']]['positions_ya'][1]++; 
		    $positions['project']['positions_ya'][1]++;
		}
		if($pos['pos_goog']>3 && $pos['pos_goog']<11 && $pos['pos_goog']<>0){
		    $positions[$pos['gr_name']]['positions_goog'][2]++;
		    $positions['project']['positions_goog'][2]++;
		}
		if($pos['pos_ya']>3 && $pos['pos_ya']<11 && $pos['pos_ya']<>0){
		    $positions[$pos['gr_name']]['positions_ya'][2]++;  
		    $positions['project']['positions_ya'][2]++;
		}
		if($pos['pos_goog']>10 || $pos['pos_goog']==0){
		    $positions[$pos['gr_name']]['positions_goog'][3]++; 
		    $positions['project']['positions_goog'][3]++;
		}
		if($pos['pos_ya']>10 || $pos['pos_ya']==0){
		    $positions[$pos['gr_name']]['positions_ya'][3]++; 
		    $positions['project']['positions_ya'][3]++;
		}
	    }
	    return $positions;
    }
    
    function get_visibles(){
	    $date_points = 1;
	    $sql_chast = "
		    SELECT SUM(  `sp`.`chast` ) as sum,  `sp`.`id_group` 
		    FROM  `srep_phrases`  `sp` 
		    INNER JOIN  `srep_group`  `sg` ON  `sg`.`id` =  `sp`.`id_group` 
		    WHERE  `sg`.`id_project` = :project_id
		    GROUP BY  `sp`.`id_group` ";

	    $connection=Yii::app()->db;
	    $command=$connection->createCommand($sql_chast);
	    $command->bindParam(":project_id",$this->id_project,PDO::PARAM_STR);
	    $sum = $command->queryAll();
	    foreach($sum as $s){
		$sum_chast[$s['id_group']] = $s['sum'];
	    }
	    $sql = "
		    SELECT * FROM `srep_index` `si`
		    INNER JOIN `srep_group` `sg` ON `sg`.`id` = `si`.`id_group`
		    INNER JOIN `srep_project` `spr` ON `spr`.`id` = `sg`.`id_project`
		    WHERE `sg`.`id_project` = :project_id
		    ORDER BY `si`.`date` DESC";
	    $connection=Yii::app()->db;
	    $command=$connection->createCommand($sql);
	    $command->bindParam(":project_id",$this->id_project,PDO::PARAM_STR);
	    $result = $command->queryAll();
	    $check_fdate = Yii::app()->dateFormatter->formatDateTime($result[0]['date'],'full',null);
	    for($i=0;$i<count($result);++$i){
		    $fdate = Yii::app()->dateFormatter->formatDateTime($result[$i]['date'],'full',null);
		    if($check_fdate<>$fdate){
			$check_fdate = Yii::app()->dateFormatter->formatDateTime($result[$i]['date'],'full',null);
			++$date_points;
		    }
		    if($date_points<3){
			$view_pos[$result[$i]['gr_name']][$date_points]['view_pos_ya'] = (int)$result[$i]['ya_index']/((int)$sum_chast[$result[$i]['id_group']]/100);
			$view_pos[$result[$i]['gr_name']][$date_points]['view_pos_goog'] = (int)$result[$i]['goog_index']/(((int)$sum_chast[$result[$i]['id_group']]*0.5)/100);
			$view_pos[$result[$i]['gr_name']][$date_points]['view_pos_sum'] = ((int)$result[$i]['goog_index']+(int)$result[$i]['ya_index'])/(((int)$sum_chast[$result[$i]['id_group']]*1.5)/100);
			$index_pr['ya'][$date_points] += (int)$result[$i]['ya_index'];
			$index_pr['goog'][$date_points] += (int)$result[$i]['goog_index'];
			
		    }else{
			break;
		    }
	    }
	    $view_pos['project'][1]['view_pos_ya'] = (int)$index_pr['ya'][1]/((int)array_sum($sum_chast)/100);
	    $view_pos['project'][1]['view_pos_goog'] = (int)$index_pr['goog'][1]/(((int)array_sum($sum_chast)*0.5)/100);
	    $view_pos['project'][1]['view_pos_sum'] = ((int)$index_pr['goog'][1]+(int)$index_pr['ya'][1])/(((int)array_sum($sum_chast)*1.5)/100);
	    $view_pos['project'][2]['view_pos_ya'] = (int)$index_pr['ya'][2]/((int)array_sum($sum_chast)/100);
	    $view_pos['project'][2]['view_pos_goog'] = (int)$index_pr['goog'][2]/(((int)array_sum($sum_chast)*0.5)/100);
	    $view_pos['project'][2]['view_pos_sum'] = ((int)$index_pr['goog'][2]+(int)$index_pr['ya'][2])/(((int)array_sum($sum_chast)*1.5)/100);
	    if(!empty($view_pos)){
		    foreach($view_pos as $gr_id=>$vp_data){
			$view_pos_rez[$gr_id]['view_pos_ya'] = round($vp_data[1]['view_pos_ya'],2);
			$view_pos_rez[$gr_id]['view_pos_goog'] = round($vp_data[1]['view_pos_goog'],2);
			$view_pos_rez[$gr_id]['view_pos_sum'] = round($vp_data[1]['view_pos_sum'],2);
			$view_pos_rez[$gr_id]['view_pos_ya_raz'] = round($vp_data[1]['view_pos_ya']-$vp_data[2]['view_pos_ya'],2);
			$view_pos_rez[$gr_id]['view_pos_goog_raz'] = round($vp_data[1]['view_pos_goog']-$vp_data[2]['view_pos_goog'],2);
			$view_pos_rez[$gr_id]['view_pos_sum_raz'] = round($vp_data[1]['view_pos_sum']-$vp_data[2]['view_pos_sum'],2);
		    }
	    }
	    return $view_pos_rez;
    }
    
}
