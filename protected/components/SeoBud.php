<?php 
include_once YiiBase::getPathOfAlias('application.components').'/xmlrpc.inc.php';
class SeoBud {

	public $current_date;
	public $last_date;
	private function get_kp($pos){
		switch($pos){
			case 1:
			case 2:
			case 3:
				$kp = 1;
				break;
			case 4:
				$kp = 0.85;
				break;
			case 5:
				$kp = 0.6;
				break;
			case 6:
			case 7:
				$kp = 0.5;
				break;	
			case 8:
			case 9:
				$kp = 0.3;
				break;
			case 10:
				$kp = 0.2;
				break;
			default:
				$kp = 0;
				break;					
		}
		return $kp;
	}		
	public function __construct($current_date, $last_date) {
		$this->seobudget_auth();
		$this->current_date = $current_date;
		$this->last_date = $last_date;
	}

	private function seobudget_auth(){
		XMLRPC_request(
			'seobudget.ru', '/api/', 'seobudget.login', 
			array(
					XMLRPC_prepare('moskvin'), 
					XMLRPC_prepare('linkmoskvin'),
				)
		);
	}

	private function sb_get_position($words=array(),$company){

		$result = XMLRPC_request(
			'seobudget.ru', '/api/', 'seobudget.getToolResultByParams', 
			array(
					XMLRPC_prepare($company), 
					XMLRPC_prepare(array( 
						'date_from'=>$this->current_date,  
						'date_to'=>$this->last_date, 
						'words'=>$words,  
						'is_only_position'=>false,
						//'is_show_serp'=>true
					)
				)
			)
		);
		return $result;
	}

	private function get_word($id_group){
		$word = array();
		//$res = mysql_query("SELECT id, id_group, phrase FROM `srep_phrases` WHERE id_group=$id_group");
		$phrases = SrepPhrases::model()->findAll('id_group='.$id_group);
		foreach($phrases as $row){
			$word['data_word']['phrase'][] = $row['phrase'];
			$word['data_word']['id'][] = $row['id'];
			$word['data_word']['id_group'][] = $row['id_group'];
			$word['words'][$row['phrase']] = $row['id'];
		}
		return $word;
	}

	function sb_set_position(){
		$k = 0;
		$groups = SrepGroup::model()->findAll('id_seobudget<>""');
		foreach($groups as $gr){
			if(!empty($gr['id_seobudget'])){
				$word = $this->get_word($gr['id']);
				list($success, $response) = $this->sb_get_position($word['data_word']['phrase'],$gr['id_seobudget']);
				if($success && !empty($response)){
					$k = 1;
					$resval = array_values($response);
					$respos = array_merge_recursive($resval[0][0]['query'],$resval[0][1]['query']);
					
					foreach($respos as $prase=>$data){
					    $position = new SrepPosition;
					    $position->attributes = array(
						'id_phrase'=>$word['words'][$prase],
						'pos_goog'=>$data['position'][0],
						'pos_ya'=>$data['position'][1],
						'date'=>Yii::app()->dateFormatter->format('yyyy.MM.dd 00:00:00', $this->current_date),
					    );
					    $position->save();
					}
				}else{
					continue;
				}
			}
		}
		if($k)$this->set_index();
	}

	private function calc_index(){
		$sql = "SELECT * FROM `srep_group` `sg`
			INNER JOIN `srep_phrases` `sph` ON `sg`.`id` = `sph`.`id_group`
			INNER JOIN `srep_position` `spos` ON `sph`.`id` = `spos`.`id_phrase`
			WHERE date = FROM_UNIXTIME('".$this->current_date."')
		";
		$connection=Yii::app()->db;
		$result=$connection->createCommand($sql)->queryAll();
		foreach($result as $data){
			$indexes[$data['id_group']]['ya'] += $data['chast']*$this->get_kp($data['pos_ya']);
			$indexes[$data['id_group']]['goog'] += $data['chast']*$this->get_kp($data['pos_goog'])*0.5;
		}
		return $indexes;
	}

	public function set_index(){
		$indexes = $this->calc_index();
		foreach($indexes as $group => $index){
			$indexC = new SrepIndex;
			$indexC->attributes = array(
			    'id_group'=>$group,
			    'ya_index'=>round($index['ya']),
			    'goog_index'=>round($index['goog']),
			    'date'=>Yii::app()->dateFormatter->format('yyyy.MM.dd 00:00:00', $this->current_date),
			);
			$indexC->save();
		}
	}

}
?>