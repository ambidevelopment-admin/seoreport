$(function () {
	$.getJSON("json/getIndexes.php", function(json){
		$.each(json.groups, function(gr_id, dataGroup) {
			var iGr=0;
			var titleGr='';
			var dataSourceGr = [];
			var appendGr = '<div id="chartContainerGroup'+gr_id+'" class="chart"></div>';
			$("#StatisticGr").append(appendGr);
			$.each(dataGroup, function(date, dataItem) {
				dataSourceGr[iGr] = dataItem;
				titleGr = dataItem.name;
				iGr++;
			});
			
			$("#chartContainerGroup"+gr_id).dxChart({
				palette:"Harmony Light",
				dataSource: dataSourceGr,
				commonSeriesSettings: {
					argumentField: "date",
					type: "spline",
					hoverMode: "allArgumentPoints",
					selectionMode: "allArgumentPoints",
					label: {
						visible: true,
						format: "fixedPoint",
						precision: 0
					}
				},
				series: [
					{ valueField: "goog", name: "Google", color: '#4485F5'},
					{ valueField: "ya", name: "Yandex", color: '#FFCC00' },
				],
				title: titleGr,
				legend: {
					verticalAlignment: "bottom",
					horizontalAlignment: "center"
				},
				pointClick: function (point) {
					this.select();
				}
			});
			
			$("#gLine").click(function() {
				var chart = $("#chartContainerGroup"+gr_id).dxChart("instance");
				chart.option({
					series: [ { valueField: "goog", name: "Google", color: '#4485F5'},]
				});
			});
			
			$("#yaLine").click(function() {
				var chart = $("#chartContainerGroup"+gr_id).dxChart("instance");
				chart.option({
					series: [ { valueField: "ya", name: "Yandex", color: '#FFCC00' },]
				});
			});

			$("#allLine").click(function() {
				var chart = $("#chartContainerGroup"+gr_id).dxChart("instance");
				chart.option({
					series: [ { valueField: "ya", name: "Yandex", color: '#FFCC00' },{ valueField: "goog", name: "Google", color: '#4485F5'}]
				});
			});
			$("#typeBar").click(function() {
				var chart = $("#chartContainerGroup"+gr_id).dxChart("instance");
				chart.option("commonSeriesSettings.type", "bar");
			});
			$("#typeLine").click(function() {
				var chart = $("#chartContainerGroup"+gr_id).dxChart("instance");
				chart.option("commonSeriesSettings.type", "spline");
			});			
			
		});
		$.each(json.projects, function(pr_id, dataProject) {
			var i=0;
			var title='';
			var dataSource = [];
			var append = '<div id="chartContainerProject'+pr_id+'" class="chart"></div>';
			$("#StatisticPr").append(append);
			$.each(dataProject, function(date, dataItem) {
				dataSource[i] = dataItem;
				title = dataItem.name;
				i++;
			});
			
			$("#chartContainerProject"+pr_id).dxChart({
				dataSource: dataSource,
				commonSeriesSettings: {
					argumentField: "date",
					type: "spline",
					hoverMode: "allArgumentPoints",
					selectionMode: "allArgumentPoints",
					label: {
						visible: true,
						format: "fixedPoint",
						precision: 0
					}
				},
				series: [
					{ valueField: "goog", name: "Google", color: '#4485F5'},
					{ valueField: "ya", name: "Yandex", color: '#FFCC00' },
				],
				title: title,
				legend: {
					verticalAlignment: "bottom",
					horizontalAlignment: "center"
				},
				pointClick: function (point) {
					this.select();
				}
			});
			
			$("#gLine").click(function() {
				var chart = $("#chartContainerProject"+pr_id).dxChart("instance");
				chart.option({
					series: [ { valueField: "goog", name: "Google", color: '#4485F5'},]
				});
			});
			
			$("#yaLine").click(function() {
				var chart = $("#chartContainerProject"+pr_id).dxChart("instance");
				chart.option({
					series: [ { valueField: "ya", name: "Yandex", color: '#FFCC00' },]
				});
			});

			$("#allLine").click(function() {
				var chart = $("#chartContainerProject"+pr_id).dxChart("instance");
				chart.option({
					series: [ { valueField: "ya", name: "Yandex", color: '#FFCC00' },{ valueField: "goog", name: "Google", color: '#4485F5'}]
				});
			});
			
			$("#typeBar").click(function() {
				var chart = $("#chartContainerProject"+pr_id).dxChart("instance");
				chart.option("commonSeriesSettings.type", "bar");
			});
			$("#typeLine").click(function() {
				var chart = $("#chartContainerProject"+pr_id).dxChart("instance");
				chart.option("commonSeriesSettings.type", "spline");
			});
		});
		
		
	});

});	